<?php

namespace Drupal\Tests\anti_duplicates\Traits;

/**
 * Simplify working with functional test settings.
 */
trait AntiDuplicatesFunctionalTestTrait {

  /**
   * A user with permission to add content and administer anti duplicate.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $sutAntiDuplicateAdminUser;

  /**
   * Creates an article node type, and a page node type.
   */
  public function setUpArticleAndPageContentTypes() {
    // Create Article content type.
    $type = $this->container->get('entity_type.manager')->getStorage('node_type')
      ->create([
        'type' => 'article',
        'name' => 'Article',
      ]);
    $type->save();

    // Create Basic Page content type.
    $type = $this->container->get('entity_type.manager')->getStorage('node_type')
      ->create([
        'type' => 'page',
        'name' => 'Basic Page',
      ]);
    $type->save();
  }

  /**
   * Create user with perms to create nodes and change anti_duplicates settings.
   */
  public function setUpAdminUser() {
    $this->sutAntiDuplicateAdminUser = $this->drupalCreateUser([
      'access content',
      'create page content',
      'create article content',
      'administer anti_duplicates',
      'administer nodes',
    ]);
  }

  /**
   * Change default displayed message in Related Content section on node page.
   */
  public function antiDuplicatesChangeFoundMessage($message) {
    $config = \Drupal::configFactory()->getEditable('anti_duplicates.settings');
    $config->set('anti_duplicates_message.value', $message);
    $config->save();
  }

  /**
   * Disable form submission if there is duplication.
   */
  public function antiDuplicatesDisableFormSubmission() {
    $config = \Drupal::configFactory()->getEditable('anti_duplicates.settings');
    $config->set('anti_duplicates_form_submission', TRUE);
    $config->save();
  }

  /**
   * Enable form submission if there is duplication.
   */
  public function antiDuplicatesEnableFormSubmission() {
    $config = \Drupal::configFactory()->getEditable('anti_duplicates.settings');
    $config->set('anti_duplicates_form_submission', FALSE);
    $config->save();
  }

  /**
   * Change the anti-duplicates detection method.
   */
  public function antiDuplicatesSearchType($newType) {
    $config = \Drupal::configFactory()->getEditable('anti_duplicates.settings');
    $config->set('anti_duplicates_search_type', $newType);
    $config->save();
  }

}
