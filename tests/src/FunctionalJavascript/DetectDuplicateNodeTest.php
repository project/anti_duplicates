<?php

namespace Drupal\Tests\anti_duplicates\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\anti_duplicates\Traits\AntiDuplicatesFunctionalTestTrait;
use Drupal\Tests\RandomGeneratorTrait;

/**
 * Test that duplicate nodes can be detected.
 *
 * @group anti_duplicates
 */
class DetectDuplicateNodeTest extends WebDriverTestBase {
  use AntiDuplicatesFunctionalTestTrait;
  use RandomGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['anti_duplicates', 'node'];

  /**
   * {@inheritdoc}
   */
  protected $sutNodeTitle;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Change anti-duplicates global settings to a known-good state for this
    // test (i.e.: so changing defaults doesn't make this test break).
    $this->antiDuplicatesChangeFoundMessage('Duplicate found');
    $this->antiDuplicatesEnableFormSubmission();
    $this->antiDuplicatesSearchType(0);

    $this->setUpArticleAndPageContentTypes();

    // Create node with a random title and body.
    $this->sutNodeTitle = $this->randomString();
    $this->drupalCreateNode([
      'type' => 'page',
      'title' => $this->sutNodeTitle,
      'body' => '',
    ]);

    $this->setUpAdminUser();
  }

  /**
   * Test the anti duplicates is working with twp different content types.
   */
  public function testCreateAnotherDifferentContentType() {
    $this->drupalLogin($this->sutAntiDuplicateAdminUser);
    $this->drupalGet('node/add/article');
    $page = $this->getSession()->getPage();

    // Check there is no duplicated title value.
    $adSimilarContentListBefore = $page->find('css', 'ul#dw-listing');
    $this->assertNull($adSimilarContentListBefore);

    // Enter the same title on the current page.
    $titleField = $page->find('css', 'input[data-drupal-selector="edit-title-0-value"]');
    $titleField->setValue($this->sutNodeTitle);
    $this->getSession()->wait(5000, 'document.querySelector("ul#dw-listing")');

    // Check that duplicated title value visible after title entered.
    $adSimilarContentListAfter = $page->find('css', 'ul#dw-listing');
    $this->assertNotNull($adSimilarContentListAfter);
  }

  /**
   * Test the anti duplicates is working with same content type.
   */
  public function testCreateAnotherSameContentType() {
    $this->drupalLogin($this->sutAntiDuplicateAdminUser);
    $this->drupalGet('node/add/page');
    $page = $this->getSession()->getPage();

    // Check there is no duplicated title value.
    $adSimilarContentListBefore = $page->find('css', 'ul#dw-listing');
    $this->assertNull($adSimilarContentListBefore);

    // Enter the same title on the current page.
    $titleField = $page->find('css', 'input[data-drupal-selector="edit-title-0-value"]');
    $titleField->setValue($this->sutNodeTitle);
    $this->getSession()->wait(5000, 'document.querySelector("ul#dw-listing")');

    // Check that duplicated title value visible after title entered.
    $adSimilarContentListAfter = $page->find('css', 'ul#dw-listing');
    $this->assertNotNull($adSimilarContentListAfter);
  }

}
