<?php

namespace Drupal\Tests\anti_duplicates\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\anti_duplicates\Traits\AntiDuplicatesFunctionalTestTrait;

/**
 * Test Anti Duplicates global configuration form.
 *
 * @group anti_duplicates
 */
class GlobalConfigurationTest extends WebDriverTestBase {
  use AntiDuplicatesFunctionalTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['anti_duplicates', 'node'];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->setUpArticleAndPageContentTypes();
    $this->setUpAdminUser();
    $this->drupalLogin($this->sutAntiDuplicateAdminUser);
  }

  /**
   * Test that we can stop the form from being submitted when duplicates exist.
   */
  public function testDisableFormSubmitWhenDuplicatesExistConfig() {
    // Change other anti-duplicates global settings to a known-good state for
    // this test (i.e.: so changing defaults for those other settings doesn't
    // break this test method).
    $this->antiDuplicatesChangeFoundMessage('Duplicate found');
    $this->antiDuplicatesSearchType(0);

    // Create a node with a random string.
    $sutNodeTitle = $this->randomString();
    $this->drupalCreateNode([
      'type' => 'page',
      'title' => $sutNodeTitle,
      'body' => '',
    ]);

    // Disable form submission.
    $this->antiDuplicatesDisableFormSubmission();

    // Load a page where anti-duplicates runs.
    $this->drupalGet('node/add/page');
    $page = $this->getSession()->getPage();

    // Check there is initially no duplicated title value.
    $adSimilarContentListBefore = $page->find('css', 'ul#dw-listing');
    $this->assertNull($adSimilarContentListBefore);

    // Populate the title field with the duplicate title.
    $titleField = $page->find('css', 'input[data-drupal-selector="edit-title-0-value"]');
    $titleField->setValue($sutNodeTitle);
    $this->getSession()->wait(5000, 'document.querySelector("ul#dw-listing")');

    // Check that duplicated title value visible after title entered.
    $adSimilarContentListAfter = $page->find('css', 'ul#dw-listing');
    $this->assertNotNull($adSimilarContentListAfter);

    // Verify that the submit button is disabled, as per the "disable form
    // submission" global preference.
    $button = $page->find('css', '#edit-submit');
    $button_disabled = $button->hasAttribute('disabled');

    // Click the "Not a duplicate" button to enable submit button, then verify
    // that it is enabled.
    $not_a_dulpicate = '#dw-enable-form';
    $this->click($not_a_dulpicate);
    $this->assertTrue($button_disabled);
  }

  /**
   * Test the different options with search type.
   */
  public function testSearchType() {
    // Change other anti-duplicates global settings to a known-good state for
    // this test (i.e.: so changing defaults for those other settings doesn't
    // break this test method).
    $this->antiDuplicatesChangeFoundMessage('Duplicate found');
    $this->antiDuplicatesEnableFormSubmission();

    // Create a variable number of random words for a title.
    $sutNodeTitle = 'ONE TWO THREE';

    // Create initial node.
    $this->drupalCreateNode([
      'type' => 'page',
      'title' => $sutNodeTitle,
      'body' => '',
    ]);

    //
    // Test search type 0: 'Contains a sequence of the title keywords': must
    // contain all words from the original in the same order, but can have words
    // between.
    //
    $this->antiDuplicatesSearchType(0);
    $this->drupalGet('node/add/page');
    $page = $this->getSession()->getPage();

    $newTitle = 'lorem ONE ipsum TWO dolor THREE sit';

    $titleField = $page->find('css', 'input[data-drupal-selector="edit-title-0-value"]');
    $titleField->setValue($newTitle);
    $this->getSession()->wait(5000, 'document.querySelector("ul#dw-listing")');
    $adSimilarContentListAfter = $page->find('css', 'ul#dw-listing');
    $this->assertNotNull($adSimilarContentListAfter);

    //
    // Test search type 1: 'Contains the exact title': must contain the exact
    // title, but can have other words before and after.
    //
    $this->antiDuplicatesSearchType(1);
    $this->drupalGet('node/add/page');
    $page = $this->getSession()->getPage();

    $newTitle = 'lorem ONE TWO THREE ipsum';

    $titleField = $page->find('css', 'input[data-drupal-selector="edit-title-0-value"]');
    $titleField->setValue($newTitle);
    $this->getSession()->wait(5000, 'document.querySelector("ul#dw-listing")');
    $adSimilarContentListAfter = $page->find('css', 'ul#dw-listing');
    $this->assertNotNull($adSimilarContentListAfter);

    //
    // Test search type 2: 'Contains any word from the title': at least one word
    // from the title is present.
    //
    $this->antiDuplicatesSearchType(2);
    $this->drupalGet('node/add/page');
    $page = $this->getSession()->getPage();

    $newTitle = 'lorem ipsum dolor TWO sit';

    $titleField = $page->find('css', 'input[data-drupal-selector="edit-title-0-value"]');
    $titleField->setValue($newTitle);
    $this->getSession()->wait(5000, 'document.querySelector("ul#dw-listing")');
    $adSimilarContentListAfter = $page->find('css', 'ul#dw-listing');
    $this->assertNotNull($adSimilarContentListAfter);
  }

  /**
   * Test that it is possible to configure the "duplicates found" message.
   */
  public function testCanChangeDuplicatesFoundMessage() {
    // Change other anti-duplicates global settings to a known-good state for
    // this test (i.e.: so changing defaults for those other settings doesn't
    // break this test method).
    $this->antiDuplicatesEnableFormSubmission();
    $this->antiDuplicatesSearchType(0);

    $sutNodeTitle = $this->randomString();
    $this->drupalCreateNode([
      'type' => 'page',
      'title' => $sutNodeTitle,
      'body' => '',
    ]);

    $newMessage = 'Duplicated content list below';
    $this->antiDuplicatesChangeFoundMessage($newMessage);

    // Load a page where the message will be shown.
    $this->drupalGet('node/add/page');
    $assertSession = $this->assertSession();

    $assertSession->pageTextContains($newMessage);
  }

}
