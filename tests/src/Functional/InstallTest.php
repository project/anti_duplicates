<?php

namespace Drupal\Tests\anti_duplicates\Functional;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Test the Anti duplicates install without errors.
 *
 * For example, if parts of the anti_duplicates config schema are
 * missing, then the module will not install correctly. Note that Unit and
 * Kernel tests don't fully install the module.
 *
 * @group anti_duplicates
 */
class InstallTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['anti_duplicates'];

  /**
   * Assert that the anti_duplicates module installed correctly.
   */
  public function testModuleInstalls() {
    // If we get here, then the module was successfully installed during the
    // setUp phase without throwing any Exceptions. Assert that TRUE is true,
    // so at least one assertion runs, and then exit.
    $this->assertTrue(TRUE, 'Module installed correctly.');
  }

}
