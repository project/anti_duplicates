(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.anti_duplicates = {
    attach: function (context, settings) {

      /*
      when the link "not a duplicate" is clicked we re-enable the submission
      inputs for the user to be able to submit the form.
       */
      $('#dw-enable-form').on('click', function(e) {
        e.preventDefault();
        $('.page-wrapper .form-submit').prop('disabled', false);
        $('#edit-anti-duplicates, #dw_container').hide();
        $('#dw-listing').empty();
      });

      // If there's no result, hide containers & reactivate form submission
      if ($('#dw_container').children().length === 0 || $('#dw-listing').length === 0 || $('#dw-listing').children().length === 0) {
        $('#edit-anti-duplicates, #dw_container').hide();
        if (drupalSettings.anti_duplicates.disable_form) {
          $('.page-wrapper .form-submit').prop('disabled', false);
        }
      } else {
        $('#edit-anti-duplicates, #dw_container').show();
        if (drupalSettings.anti_duplicates.disable_form) {
          $('.page-wrapper .form-submit').prop('disabled', true);
        }
      }

    }
  };

  // Courtesy of https://www.drupal.org/project/drupal/issues/2984848.
  Drupal.behaviors.keyupDebounced = {
    attach: function (context, settings) {
      $('.delayed-input-submit').each(function () {
        var $self = $(this);
        var timeout = null;
        var delay = $self.data('delay') || 1000;
        var triggerEvent = $self.data('event') || "keyup_debounced";

        $self.unbind('keyup').keyup(function () {
          clearTimeout(timeout);
          timeout = setTimeout(function () {
            $self.trigger(triggerEvent);
          }, delay);
        });
      });
    }
  }  

})(jQuery, Drupal, drupalSettings);
