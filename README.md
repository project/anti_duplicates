## Anti Duplicates

Anti-Duplicates is a module that helps you avoid duplicate content on
your website by displaying possible "duplicates" for the content you
are posting, based on the keyword on the title of that content.

The module also offers the "Disable form submission" option, which
allows the user to disable the form submission, and only re-enable
it when agreeing that the content is unique, by clicking on a
"Not a duplicate" link.

### Features

* Disable automatic form submission until you click on 'not a duplicate'
button.
* Provides various search type options for duplicate content lookup.
* Select which content types you want to check for duplicates.
* Placement of duplicate results and information is configurable to right
sidebar or below title field.
* Only show duplicate results if they are found.

### Requirements

No requirements at this time.

### Install/Usage

* Install like any other contributed module.
* Configure settings for module: /admin/config/anti-duplicates
Add or edit existing content. Adjust title or enter a title. If any duplicate
results, they should now appear for you.

### Maintainers

* George Anderson (geoanders) : https://www.drupal.org/u/geoanders
