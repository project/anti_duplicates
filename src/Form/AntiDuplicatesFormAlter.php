<?php

namespace Drupal\anti_duplicates\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;

/**
 * Hooks into all node forms.
 */
class AntiDuplicatesFormAlter {

  /**
   * Search for duplicates based on the title of the node being created.
   */
  public static function duplicatesSearch(array &$form, FormStateInterface $form_state) {

    $config = \Drupal::config('anti_duplicates.settings');

    // Get the node title.
    $title = $form_state->getValue('title')[0]['value'];
    $title = trim($title);

    $currentNode = $form_state->getFormObject()->getEntity();
    assert($currentNode instanceof NodeInterface);

    $nodes = [];
    $count = 0;

    // Add an Ajax response.
    $response = new AjaxResponse();

    $contentTypes = $config->get('anti_duplicates_content_types');
    if (!in_array($currentNode->getType(), $contentTypes, TRUE)) {
      return $response;
    }

    if ($title) {
      // Get the search type.
      switch ($config->get('anti_duplicates_search_type')) {
        // Contains a sequence of the title keywords.
        case 0:
          $title = '%' . str_replace(' ', '%', $title) . '%';
          $query = \Drupal::entityQuery('node')
            ->condition('title', $title, 'LIKE')
            ->condition('type', $currentNode->getType())
            ->accessCheck();

          if (!$currentNode->isNew()) {
            $query->condition('nid', $currentNode->id(), '!=');
          }

          $count = (clone $query)->count()->execute();
          $nids = (clone $query)->range(0, 5)->execute();
          $nodes = Node::loadMultiple($nids);
          break;

        // Contains the exact title.
        case 1:
          $title = '%' . $title . '%';
          $query = \Drupal::entityQuery('node')
            ->accessCheck()
            ->condition('title', $title, 'LIKE')
            ->condition('type', $currentNode->getType());

          if (!$currentNode->isNew()) {
            $query->condition('nid', $currentNode->id(), '!=');
          }

          $count = (clone $query)->count()->execute();
          $nids = (clone $query)->range(0, 5)->execute();
          $nodes = Node::loadMultiple($nids);
          break;

        // Contains any word from the title.
        case 2:
          $titleArray = explode(' ', $title);
          $query = \Drupal::entityQuery('node')
            ->accessCheck()
            ->condition('type', $currentNode->getType());

          if (!$currentNode->isNew()) {
            $query->condition('nid', $currentNode->id(), '!=');
          }

          $orGroup = $query->orConditionGroup();
          foreach ($titleArray as $value) {
            $orGroup->condition('title', '%' . $value . '%', 'LIKE');
          }

          $count = (clone $query)->count()->execute();
          $nids = (clone $query)->range(0, 5)->execute();
          $nodes = Node::loadMultiple($nids);
          break;
      }
    }

    $show = $config->get('anti_duplicates_display_not_zero');

    // Append a list of found results.
    $response->addCommand(new RemoveCommand('#dw-listing'));
    $response->addCommand(new AppendCommand('#dw_container', '<ul id="dw-listing"></ul>'));

    $txt = t('Total');
    $response->addCommand(new RemoveCommand('#dw-total'));

    if (!$count && !$show) {
      return $response;
    }

    $response->addCommand(new AppendCommand('#dw_container', '<span id="dw-total">' . $txt . ' : <b>' . $count . '</b></span>'));

    // List all possible duplicates found.
    foreach ($nodes as $nid => $node) {
      $options = ['absolute' => TRUE];
      $url = Url::fromRoute('entity.node.canonical', ['node' => $nid], $options);
      $url = '<a href="' . $url->toString() . '" title="' . $node->get('title')->value . '" target="_blank">' . $node->get('title')->value . '</a>';
      $response->addCommand(new AppendCommand('#dw-listing', '<li>' . $url . '</li>'));
    }

    // Add a link to the anti-duplicates form when the option disable form is
    // checked, this link allows the user to enable the form submission by
    // agreeing that the content is unique.
    if ($config->get('anti_duplicates_form_submission') && intval($count) > 0) {
      $response->addCommand(new RemoveCommand('#dw-enable-form'));
      $btn = '<a href="#" class="button" id="dw-enable-form">' . t('Not a duplicate') . '</a>';
      $response->addCommand(new AppendCommand('#dw_container', $btn));
    }
    else {
      $response->addCommand(new RemoveCommand('#dw-enable-form'));
    }

    return $response;
  }

}
