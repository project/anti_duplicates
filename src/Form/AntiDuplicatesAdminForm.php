<?php

namespace Drupal\anti_duplicates\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Anti duplicates admin form.
 */
class AntiDuplicatesAdminForm extends ConfigFormBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Service injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container object.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'anti_duplicates_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['anti_duplicates.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);
    $config = $this->config('anti_duplicates.settings');

    $form['anti_duplicates_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Message'),
      '#default_value' => $config->get('anti_duplicates_message.value'),
      '#format' => $config->get('anti_duplicates_message.format'),
      '#description' => $this->t("The notice message that shows on the add content page. If not set, a default text will be displayed."),
    ];

    $form['anti_duplicates_form_submission'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable form submission'),
      '#description' => $this->t("Disable automatic form submission until you click on 'not a duplicate' button."),
      '#default_value' => $config->get('anti_duplicates_form_submission'),
    ];

    $form['anti_duplicates_search_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Search type'),
      '#default_value' => $config->get('anti_duplicates_search_type'),
      '#options' => [
        0 => $this->t('Contains a sequence of the title keywords'),
        1 => $this->t('Contains the exact title'),
        2 => $this->t('Contains any word from the title'),
      ],
      '#required' => TRUE,
      '#description' => $this->t("Choose the way Anti-Duplicates search for possible duplicates."),
    ];

    $types = [];
    $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach ($content_types as $content_type) {
      $types[$content_type->id()] = $content_type->label();
    }
    $form['anti_duplicates_content_types'] = [
      '#type' => 'checkboxes',
      '#options' => $types,
      '#title' => $this->t('Content types'),
      '#default_value' => !empty($config->get('anti_duplicates_content_types')) ? $config->get('anti_duplicates_content_types') : [
        'article',
        'page',
      ],
      '#description' => $this->t('Select content types you want to use the anti duplicates for. If none are selected, no restriction based on types will be implemented.'),
    ];

    $form['anti_duplicates_placement'] = [
      '#type' => 'radios',
      '#title' => $this->t('Placement of notice message'),
      '#options' => [
        0 => $this->t('Right sidebar'),
        1 => $this->t('Under the title'),
      ],
      '#description' => $this->t('Choose where the notice message will be placed.'),
      '#default_value' => $config->get('anti_duplicates_placement') ?? 0,
    ];

    $form['anti_duplicates_display_not_zero'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display related content only in case of more than 0 results'),
      '#default_value' => $config->get('anti_duplicates_display_not_zero'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get submitted values.
    $values = $form_state->getValues();

    // Save the configuration.
    $this->config('anti_duplicates.settings')
      ->set('anti_duplicates_message', $values['anti_duplicates_message'])
      ->set('anti_duplicates_form_submission', $values['anti_duplicates_form_submission'])
      ->set('anti_duplicates_search_type', $values['anti_duplicates_search_type'])
      ->set('anti_duplicates_content_types', is_array($values['anti_duplicates_content_types']) ? $values['anti_duplicates_content_types'] : [])
      ->set('anti_duplicates_placement', $values['anti_duplicates_placement'])
      ->set('anti_duplicates_display_not_zero', $values['anti_duplicates_display_not_zero'])
      ->save();

    // Display success message.
    $this->messenger()
      ->addMessage($this->t('Anti Duplicates configuration submitted successfully.'), 'status', TRUE);
  }

}
